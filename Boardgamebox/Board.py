import random

from Boardgamebox.State import State
from Constants.Cards import playerSets
from Constants.Cards import policies


class Board(object):
    def __init__(self, playercount, game):
        self.state = State()
        self.num_players = playercount
        self.fascist_track_actions = playerSets[self.num_players]["track"]
        self.policies = random.sample(policies, len(policies))
        self.game = game
        self.discards = []
        self.previous = []

    def print_board(self):
        board = "--- Акты Либералов ---\n"
        for i in range(5):
            if i < self.state.liberal_track:
                board += u"\u2716\uFE0F" + " " #X
            elif i >= self.state.liberal_track and i == 4:
                board += u"\U0001F54A" + " " #dove
            else:
                board += u"\u25FB\uFE0F" + " " #empty
        board += "\n--- Акты Фашистов ---\n"
        for i in range(6):
            if i < self.state.fascist_track:
                board += u"\u2716\uFE0F" + " " #X
            else:
                action = self.fascist_track_actions[i]
                if action == None:
                    board += u"\u25FB\uFE0F" + " "  # empty
                elif action == "policy":
                    board += u"\U0001F52E" + " " # crystal
                elif action == "inspect":
                    board += u"\U0001F50E" + " " # inspection glass
                elif action == "kill":
                    board += u"\U0001F5E1" + " " # knife
                elif action == "win":
                    board += u"\u2620" + " " # skull
                elif action == "choose":
                    board += u"\U0001F454" + " " # tie

        board += "\n--- Подсчет голосов ---\n"
        for i in range(3):
            if i < self.state.failed_votes:
                board += u"\u2716\uFE0F" + " " #X
            else:
                board += u"\u25FB\uFE0F" + " " #empty

        board += "\n--- Порядок президентства  ---\n"
        for player in self.game.player_sequence:
            board += player.name + " " + u"\u27A1\uFE0F" + " "
        board = board[:-3]
        board += u"\U0001F501"
        board += "\n\nКарт законов в колоде: " + str(len(self.policies))
        if self.state.fascist_track >= 3:
            board += "\n\n" + u"\u203C\uFE0F" + " Будьте осторожны: Если Гитлера выберут в Канцлеры, то Фашисты победят " + u"\u203C\uFE0F"
        if len(self.state.not_hitlers) > 0:
            board += "\n\nИзвестно, что следующие игроки не являются Гитлером, потому что они избирались в Канцлеры " \
                     "после трёх фашистских законов:\n "
            for nh in self.state.not_hitlers:
                board += nh.name + ", "
            board = board[:-2]
        return board
