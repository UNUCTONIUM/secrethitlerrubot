import datetime
import json
import logging as log

from telegram import ParseMode

import GamesController
import MainController
from Boardgamebox.Board import Board
from Boardgamebox.Game import Game
from Boardgamebox.Player import Player
from Constants.Config import ADMIN
from Constants.Config import STATS

# Enable logging
log.basicConfig(level=log.INFO)


commands = [  # command description used in the "help" command
    '/help - Предоставит доступные команды',
    '/start - Расскажет пару слов об игре Secret Hitler',
    '/symbols - Покажет все доступные символы на игровой доске',
    '/rules - Предоставит ссылку на правила игры (#todo удалить)',
    '/newgame - Создаст новую игру',
    '/join - Добавит в списки игроков',
    '/startgame - Начинает игру',
    '/cancelgame - Завершает принудительно текущую игру',
    '/board - Отобразит доску с надлежащей информацией',
    '/votes - Покажет голосовавших',
    '/calltovote - Призывает игроков к голосованию'
]

symbols = [
    u"\u25FB\uFE0F" + ' Пустое поле без специальных действий',
    u"\u2716\uFE0F" + ' Поле, покрытое картой',  # X
    u"\U0001F52E" + ' Способность президента: Policy Peek',  # crystal
    u"\U0001F50E" + ' Способность президента: Investigate Loyalty',  # inspection glass
    u"\U0001F5E1" + ' Способность президента: Execution',  # knife
    u"\U0001F454" + ' Способность президента: Call Special Election',  # tie
    u"\U0001F54A" + ' Либералы победили',  # dove
    u"\u2620" + ' Фашисты победили'  # skull
]


def command_symbols(bot, update):
    cid = update.message.chat_id
    symbol_text = "Следующие символы могут быть на доске: \n"
    for i in symbols:
        symbol_text += i + "\n"
    bot.send_message(cid, symbol_text)


def command_board(bot, update):
    cid = update.message.chat_id
    if cid in GamesController.games.keys():
        if GamesController.games[cid].board:
            bot.send_message(cid, GamesController.games[cid].board.print_board())
        else:
            bot.send_message(cid, "Игра не начата. Начните игру с помощью /startgame")
    else:
        bot.send_message(cid, "Ни одна игра не запущена здесь. Вы можете создать игру с помощью /newgame")


def command_start(bot, update):
    cid = update.message.chat_id
    bot.send_message(cid,
                     '''
Секретный Гитлер - игра о политических интригах в Германии 1930-хх годов.
 
Игроки делятся на либеральную и национал-социалистические партии по скрытым ролям, одна из которых - роль Гитлера.
В ход один из игроков выбирает другого игрока канцлером, после чего все начинают голосовать за или против этого выбора. Если голосование проходит успешно, канцлер тянет 2 карты резолюций (бывают, соответственно, либеральными или фашистскими), и выбирает из них одну.
Либералы побеждают, если выложено 5 "хороших" резолюций. Фашисты побеждают, если выложено 6 фашистских резолюций, или если выложено 3 а канцлером избран Гитлер.''')
    command_help(bot, update)


# pings the bot
def command_ping(bot, update):
    cid = update.message.chat_id
    bot.send_message(cid, 'pong - v0.4')


# prints statistics, only ADMIN
def command_stats(bot, update):
    cid = update.message.chat_id
    if cid == ADMIN:
        with open(STATS, 'r') as f:
            stats = json.load(f)
        stattext = "+++ Статистика +++\n" + \
                   "Побед Либералов (законы): " + str(stats.get("libwin_policies")) + "\n" + \
                   "Побед Либералов (убийство Гитлера): " + str(stats.get("libwin_kill")) + "\n" + \
                   "Побед Фашистов (законы): " + str(stats.get("fascwin_policies")) + "\n" + \
                   "Побед Фашистов (избирание Гитлера): " + str(stats.get("fascwin_hitler")) + "\n" + \
                   "Игр отменено: " + str(stats.get("cancelled")) + "\n\n" + \
                   "Количество групп всего: " + str(len(stats.get("groups"))) + "\n" + \
                   "Запущено игр: "
        bot.send_message(cid, stattext)


# help page
def command_help(bot, update):
    cid = update.message.chat_id
    help_text = "Доступны следующие команды:\n"
    for i in commands:
        help_text += i + "\n"
    bot.send_message(cid, help_text)


def command_newgame(bot, update):
    cid = update.message.chat_id
    game = GamesController.games.get(cid, None)
    groupType = update.message.chat.type
    if groupType not in ['group', 'supergroup']:
        bot.send_message(cid, "Для начала следует добавить меня в группу, а уже там набрать эту команду!")
    elif game:
        bot.send_message(cid, "Здесь уже запущена игра. Если вы хотите ее отменить, напишите /cancelgame!")
    else:
        GamesController.games[cid] = Game(cid, update.message.from_user.id)
        with open(STATS, 'r') as f:
            stats = json.load(f)
        if cid not in stats.get("groups"):
            stats.get("groups").append(cid)
            with open(STATS, 'w') as f:
                json.dump(stats, f)
        bot.send_message(cid,
                         "Новая игра создана! Теперь игроки должны присоединиться (/join) к игре.\nСоздатель игры (или админ) может также присоединиться и начать игру, когда все будут готовы (/startgame)")


def command_join(bot, update):
    groupName = update.message.chat.title
    cid = update.message.chat_id
    groupType = update.message.chat.type
    game = GamesController.games.get(cid, None)
    fname = update.message.from_user.first_name

    if groupType not in ['group', 'supergroup']:
        bot.send_message(cid, "Для начала следует добавить меня в группу, а уже там набрать эту команду!")
    elif not game:
        bot.send_message(cid, "Ни одна игра не запущена здесь. Вы можете создать игру с помощью /newgame")
    elif game.board:
        bot.send_message(cid, "К сожалению, игра уже идёт! Придётся подождать ее завершения")
    elif update.message.from_user.id in game.playerlist:
        bot.send_message(game.cid, "Вы уже в игре, %s!" % fname)
    elif len(game.playerlist) >= 10:
        bot.send_message(game.cid,
                         "Вы достигли максимального количества игроков. Пора начинать игру с помощью /startgame!")
    else:
        uid = update.message.from_user.id
        player = Player(fname, uid)
        try:
            bot.send_message(uid,
                             "Вы присоединились к игре в группе %s. После начала игры я сообщу вам вашу роль." % groupName)
            game.add_player(uid, player)
        except Exception:
            bot.send_message(game.cid,
                             fname + ", я не могу отправить вам сообщение. Пожалуйста, нажмите [СЮДА](t.me/secrethitlerrubot?start=start), чтобы запустить меня",
                             parse_mode='markdown')
        else:
            log.info("%s (%d) joined a game in %d" % (fname, uid, game.cid))
            if len(game.playerlist) > 4:
                bot.send_message(game.cid,
                                 fname + " присоединился к игре. Напишите /startgame, если вы хотите начать игру с %d игроками!" % len(
                                     game.playerlist))
            elif len(game.playerlist) == 1:
                bot.send_message(game.cid,
                                 "%s присоединился к игре. Сейчас в игре: %d. Для старта необходимо 5-10 игроков." % (
                                     fname, len(game.playerlist)))
            else:
                bot.send_message(game.cid,
                                 "%s присоединился к игре. Сейчас в игре: %d. Для старта необходимо 5-10 игроков." % (
                                     fname, len(game.playerlist)))


def command_startgame(bot, update):
    log.info('command_startgame called')
    cid = update.message.chat_id
    game = GamesController.games.get(cid, None)
    if not game:
        bot.send_message(cid, "Ни одна игра не запущена здесь. Вы можете создать игру с помощью /newgame")
    elif game.board:
        bot.send_message(cid, "Игра уже в процессе!")
    elif update.message.from_user.id != game.initiator and bot.getChatMember(cid, update.message.from_user.id).status not in ("administrator", "creator"):
        bot.send_message(game.cid, "Только создатель игры или администратор может использовать /startgame")
    elif len(game.playerlist) < 5:
        bot.send_message(game.cid,
                         "Маловато игроков для начала игры (мин. 5, макс. 10). Присоединяйтесь в игру при помощи /join")
    else:
        player_number = len(game.playerlist)
        MainController.inform_players(bot, game, game.cid, player_number)
        MainController.inform_fascists(bot, game, player_number)
        game.board = Board(player_number, game)
        log.info(game.board)
        log.info("len(games) Command_startgame: " + str(len(GamesController.games)))
        game.shuffle_player_sequence()
        game.board.state.player_counter = 0
        bot.send_message(game.cid, game.board.print_board())
        #group_name = update.message.chat.title
        #bot.send_message(ADMIN, "Game of Secret Hitler started in group %s (%d)" % (group_name, cid))
        MainController.start_round(bot, game)

def command_cancelgame(bot, update):
    log.info('command_cancelgame called')
    cid = update.message.chat_id
    if cid in GamesController.games.keys():
        game = GamesController.games[cid]
        status = bot.getChatMember(cid, update.message.from_user.id).status
        if update.message.from_user.id == game.initiator or status in ("administrator", "creator"):
            MainController.end_game(bot, game, 99)
        else:
            bot.send_message(cid, "Только создатель игры или администратор может использовать /startgame")
    else:
        bot.send_message(cid, "Ни одна игра не запущена здесь. Вы можете создать игру с помощью /newgame")


def command_votes(bot, update):
    try:
        #Send message of executing command
        cid = update.message.chat_id
        #bot.send_message(cid, "Looking for history...")
        #Check if there is a current game
        if cid in GamesController.games.keys():
            game = GamesController.games.get(cid, None)
            if not game.dateinitvote:
                # If date of init vote is null, then the voting didnt start
                bot.send_message(cid, "Голосование еще не началось.")
            else:
                #If there is a time, compare it and send history of votes.
                start = game.dateinitvote
                stop = datetime.datetime.now()
                elapsed = stop - start
                if elapsed > datetime.timedelta(minutes=1):
                    history_text = "История голосований за Президента %s и Канцлера %s:\n\n" % (
                        game.board.state.nominated_president.name, game.board.state.nominated_chancellor.name)
                    for player in game.player_sequence:
                        # If the player is in the last_votes (He voted), mark him as he registered a vote
                        if player.uid in game.board.state.last_votes:
                            history_text += "%s зарегистрировал(а) голос.\n" % (game.playerlist[player.uid].name)
                        else:
                            history_text += "%s не зарегистрировал(а) голос.\n" % (game.playerlist[player.uid].name)
                    bot.send_message(cid, history_text)
                else:
                    bot.send_message(cid,
                                     "Не менее пяти минут должно пройти, прежде чем вы просмотрите результаты голосований")
        else:
            bot.send_message(cid, "Ни одна игра не запущена здесь. Вы можете создать игру с помощью /newgame")
    except Exception as e:
        bot.send_message(cid, str(e))


def command_calltovote(bot, update):
    try:
        #Send message of executing command
        cid = update.message.chat_id
        #bot.send_message(cid, "Looking for history...")
        #Check if there is a current game
        if cid in GamesController.games.keys():
            game = GamesController.games.get(cid, None)
            if not game.dateinitvote:
                # If date of init vote is null, then the voting didnt start
                bot.send_message(cid, "Голосование еще не началось.")
            else:
                #If there is a time, compare it and send history of votes.
                start = game.dateinitvote
                stop = datetime.datetime.now()
                elapsed = stop - start
                if elapsed > datetime.timedelta(minutes=1):
                    # Only remember to vote to players that are still in the game
                    history_text = ""
                    for player in game.player_sequence:
                        # If the player is not in last_votes send him reminder
                        if player.uid not in game.board.state.last_votes:
                            history_text += "Пора голосовать, [%s](tg://user?id=%d).\n" % (
                                game.playerlist[player.uid].name, player.uid)
                    bot.send_message(cid, text=history_text, parse_mode=ParseMode.MARKDOWN)
                else:
                    bot.send_message(cid,
                                     "Не менее пяти минут должно пройти, прежде чем вы просмотрите результаты голосований")
        else:
            bot.send_message(cid, "Ни одна игра не запущена здесь. Вы можете создать игру с помощью /newgame")
    except Exception as e:
        bot.send_message(cid, str(e))
